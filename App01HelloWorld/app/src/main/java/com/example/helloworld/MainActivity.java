package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText num1;
    private EditText num2;
    private TextView resultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        num1 = (EditText) findViewById(R.id.numero1);
        num2 = (EditText) findViewById(R.id.numero2);
        resultado = (TextView) findViewById(R.id.resultado);
/*
        int matematicas = 5;
        int quimica = 5;
        int fisica = 5;
        int promedio = 0;
        String mensaje = "";

        promedio = (matematicas + quimica + fisica) / 3;

        if (promedio >= 6) {
            mensaje = "Aprobado";
        } else {
            mensaje = "Reprobado";
        }

        //Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();

        Toast.makeText(this, ((promedio < 6) ? "Reprobado" : "Aprobado"), Toast.LENGTH_LONG).show();

 */
    }

    public void Sumar(View view) {
        String val1 = num1.getText().toString();
        String val2 = num2.getText().toString();

        int num1 = val1.isEmpty() ? 0 : Integer.parseInt(val1);
        int num2 = val2.isEmpty() ? 0 : Integer.parseInt(val2);

        int suma = num1 + num2;

        String res = String.valueOf(suma);

        resultado.setText(res);

        return;

    }
}