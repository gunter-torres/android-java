package com.example.app13archivos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.PrivateKey;

/**
 * Video 28 del curso Android desde Cero
 * de la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etDatos;
    private static final String NOMBRE_ARCHIVO = "bitacora.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etDatos = findViewById(R.id.etDatos);
        String mensaje ;
        String contenido = "";
        String[] archivos = fileList();

        if (verificarArchivo(archivos, NOMBRE_ARCHIVO)) {
            try {

                InputStreamReader archivo = new InputStreamReader(openFileInput(NOMBRE_ARCHIVO));
                BufferedReader buffer = new BufferedReader(archivo);
                String linea = buffer.readLine();

                while (linea != null) {
                    contenido += linea + "\n";
                    linea = buffer.readLine();
                }

                buffer.close();
                archivo.close();

                etDatos.setText(contenido);
            } catch (IOException e) {
                mensaje = "Error al buscar el archivo" + e.getMessage();
                Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
            }
        }
/*
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        height = Math.round(displayMetrics.heightPixels / displayMetrics.density);

        width = Math.round(displayMetrics.widthPixels / displayMetrics.density);

        mensaje = "Alto: "+height+"\n";
        mensaje += "Ancho: "+ width;

        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
*/

    }

    /**
     *
     * @param files
     * @param nombre
     * @return boolean
     */
    private boolean verificarArchivo(String[] files, String nombre) {

        int cantidad = files.length;

        for (int i = 0; i < cantidad; i++) {
            if (nombre.equals(files[i])) {
                return true;
            }
        }
        return false;
    }

    public void guardar(View view){
        String mensaje ;
        try {
            OutputStreamWriter archivo = new OutputStreamWriter(
                    openFileOutput(NOMBRE_ARCHIVO, Activity.MODE_PRIVATE)
            );

            archivo.write(etDatos.getText().toString());
            archivo.flush();
            archivo.close();
            mensaje = "Datos almacenados correctamente";
        }catch (IOException e){
            mensaje = "Error al escribir el archivo" + e.getMessage();
        }

        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
        finish();
    }
}