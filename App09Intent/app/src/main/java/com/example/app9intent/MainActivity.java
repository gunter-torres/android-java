package com.example.app9intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Video 23 de curso de Android desde cero
 * de la Geekpedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void siguiente(View view){
        Intent intent = new Intent(this, SegundoActivity.class);

       startActivity(intent);
    }
}