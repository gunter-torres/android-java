package com.example.app4checkbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText valor1;
    private EditText valor2;
    private CheckBox chkSuma;
    private CheckBox chkResta;
    private TextView texto;

    private Double getData(EditText campo) {
        String valor = campo.getText().toString();

        return valor.isEmpty() ? 0 : Double.parseDouble(valor);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valor1 = findViewById(R.id.txtValor1);
        valor2 = findViewById(R.id.txtValor2);
        chkSuma = findViewById(R.id.chkSuma);
        chkResta = findViewById(R.id.chkResta);
        texto = findViewById(R.id.tvTexto);
    }

    public void Calcular(View view) {
        Double val1 = getData(valor1);
        Double val2 = getData(valor2);
        Double res;
        String mensaje = "";

        if (chkSuma.isChecked()) {
            res = val1 + val2;
            mensaje = "La suma es: " + res;
        }

        if (chkResta.isChecked()) {
            res = val1 - val2;

            if (chkSuma.isChecked()) {
                mensaje += " \n";
            }
            mensaje += "La resta es: " + res;
        }

        texto.setText(mensaje);
    }
}