package com.example.app7imagebutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * Video 21 del Curso Android desde Cero
 * De la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mensajeAndroid(View view){
        Toast.makeText(this, "Clic en Android" ,Toast.LENGTH_LONG).show();

    }

    public void mensajeContacto(View view){
        Toast.makeText(this, "Sin contactos" ,Toast.LENGTH_LONG).show();

    }
}