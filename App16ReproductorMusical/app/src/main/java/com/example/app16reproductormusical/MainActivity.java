package com.example.app16reproductormusical;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;

import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Videos 40 y 41 del Curso Android desde Cero
 * de la Geekipedia de Ernesto
 *
 * Música:
 *  1. Piano Version, in B by Monk Turner + Fascinoma
 *  2. Piano Version, in C by Monk Turner + Fascinoma
 *  3. Happy Birthday, It's Your Day! by BrokeMC
 *  4. Birthday Song by McCloud Zicmuse
 * Descargado de:
 *  https://freemusicarchive.org/music/Happy_Birthday_Song_Contest/The_New_Birthday_Song_Contest/
 *
 * Licencia:
 *  https://creativecommons.org/licenses/by/3.0/
 */
public class MainActivity extends AppCompatActivity {

    private static final int TOTAL = 4;
    private int repetir = 2;
    private int cancion = 0;

    private Drawable icoReproducir;
    private Drawable icoCiclo;
    private Drawable icoLimpiar;
    private Drawable icoDetener;
    private Drawable icoPausa;

    MediaPlayer[] reproductor = new MediaPlayer[TOTAL];
    int[] portada = new int[TOTAL];

    ImageButton btnReproducir;
    ImageButton btnRepetir;
    ImageView ivPortada;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnReproducir = findViewById(R.id.btnReproducir);
        btnRepetir = findViewById(R.id.btnRepetir);
        ivPortada = findViewById(R.id.ivPortada);

        cancion = 0;
        iniciarCanciones();
        iniciarIconos();
        iniciarPortadas();
    }

    private void iniciarCanciones() {

        reproductor[0] = MediaPlayer.create(this, R.raw.monk_turner_fascinoma_piano_version_in_b);
        reproductor[1] = MediaPlayer.create(this, R.raw.monk_turner_fascinoma_piano_version_in_c);
        reproductor[2] = MediaPlayer.create(this, R.raw.brokemc_happy_birthday_it_s_your_day);
        reproductor[3] = MediaPlayer.create(this, R.raw.mccloud_zicmuse_birthday_song);

    }

    private void iniciarIconos() {

        icoReproducir = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_play_circle_filled_50);
        icoCiclo = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_loop_50);
        icoLimpiar = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_clear_50);
        icoDetener = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_stop_circle_50);
        icoPausa = AppCompatResources.getDrawable(this, R.drawable.ic_baseline_pause_circle_filled_50);

    }

    private void iniciarPortadas() {

        portada[0] = R.drawable.ic_baseline_music_video_330_white;
        portada[1] = R.drawable.ic_baseline_music_video_330_blue;
        portada[2] = R.drawable.ic_baseline_music_video_330_green;
        portada[3] = R.drawable.ic_baseline_music_video_330_yellow;

    }

    public void reproducir(View view) {
        if (reproductor[cancion].isPlaying()) {
            reproductor[cancion].pause();
            btnReproducir.setImageDrawable(icoReproducir);
        } else {
            reproductor[cancion].start();
            btnReproducir.setImageDrawable(icoPausa);
        }
    }

    public void detener(View view) {

        if (null != reproductor[cancion]) {
            reproductor[cancion].stop();
            btnReproducir.setImageDrawable(icoReproducir);
            ivPortada.setImageResource(portada[0]);

            cancion = 0;
            iniciarCanciones();
        }
    }

    public void repetir(View view) {

        if (1 == repetir) {
            repetir = 2;
            btnRepetir.setImageDrawable(icoCiclo);
            reproductor[cancion].setLooping(false);
        } else {
            repetir = 1;
            btnRepetir.setImageDrawable(icoLimpiar);
            reproductor[cancion].setLooping(true);
        }
    }

    public void siguiente(View view) {

        boolean reproducir = false;
        String mensaje = "";

        if (cancion < TOTAL - 1) {

            if (reproductor[cancion].isPlaying()) {
                reproductor[cancion].stop();
                reproducir = true;
            }

            cancion++;
            ivPortada.setImageResource(portada[cancion]);

            if (reproducir) {
                reproductor[cancion].start();
            }

        } else {
            mensaje = "Última canción";
            Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
        }
    }

    public void anterior(View view) {

        boolean reproducir = false;
        String mensaje = "";

        if (cancion >= 1) {

            if (reproductor[cancion].isPlaying()) {
                reproductor[cancion].stop();
                reproducir = true;
            }

            iniciarCanciones();
            cancion--;
            ivPortada.setImageResource(portada[cancion]);

            if (reproducir) {
                reproductor[cancion].start();
            }

        } else {
            mensaje = "Primera canción";
            Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
        }
    }
}