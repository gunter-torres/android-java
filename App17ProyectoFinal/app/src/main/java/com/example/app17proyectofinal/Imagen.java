package com.example.app17proyectofinal;

import android.content.Context;
import android.widget.ImageView;

public class Imagen {

    private static final String[] IC_FRUTAS = new String[]{
            "ic_fruta_mango",
            "ic_fruta_naranja",
            "ic_fruta_manzana",
            "ic_fruta_sandia",
            "ic_fruta_uva"
    };

    private static final String[] IC_NUMEROS = new String[]{
            "ic_numero_cero",
            "ic_numero_uno",
            "ic_numero_dos",
            "ic_numero_tres",
            "ic_numero_cuatro",
            "ic_numero_cinco",
            "ic_numero_seis",
            "ic_numero_siete",
            "ic_numero_ocho",
            "ic_numero_nueve"
    };

    private static final String[] IC_VIDAS = new String[]{
            "ic_vidas_una",
            "ic_vidas_dos",
            "ic_vidas_tres"
    };

    private static final String[] IC_OPERACION = new String[]{
            "ic_operacion_suma",
            "ic_operacion_resta",
            "ic_operacion_multiplica"
    };

    public static void ponerFondo(Context context, ImageView iv) {

        int aleatorio = (int) (Math.random() * 10) % 5;
        int id = context
                .getResources()
                .getIdentifier(IC_FRUTAS[aleatorio], "drawable", context.getPackageName());

        iv.setImageResource(id);

    }

    public static void ponerNumero(Context context, ImageView iv, int numero) {

        int id = context
                .getResources()
                .getIdentifier(IC_NUMEROS[numero], "drawable", context.getPackageName());

        iv.setImageResource(id);

    }

    public static void ponerIntentos(Context context, ImageView iv, int numero) {

        numero -= 1;

        if (numero >= 0) {
            int id = context
                    .getResources()
                    .getIdentifier(IC_VIDAS[numero], "drawable", context.getPackageName());

            iv.setImageResource(id);
        }

    }

    public static void ponerOperacion(Context context, ImageView iv, String operacion) {

        String op = "";

        if ("+" == operacion) {
            op = IC_OPERACION[0];
        }
        if ("-" == operacion) {
            op = IC_OPERACION[1];
        }
        if ("*" == operacion) {
            op = IC_OPERACION[2];
        }

        int id = context
                .getResources()
                .getIdentifier(op, "drawable", context.getPackageName());

        iv.setImageResource(id);


    }


}
