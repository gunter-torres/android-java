package com.example.app17proyectofinal;

import android.media.MediaPlayer;
import android.content.Context;

public class Sonido {

    public static MediaPlayer iniciarFondo(Context context, boolean segundaCancion) {
        MediaPlayer cancion = new MediaPlayer();

        if (segundaCancion) {
            cancion = MediaPlayer.create(context, R.raw.goats);
        } else {
            cancion = MediaPlayer.create(context, R.raw.alphabet_song);
        }

        cancion.start();
        cancion.setLooping(true);

        return cancion;

    }

    public static void detenerFondo(MediaPlayer cancion) {

        if (null != cancion || cancion.isPlaying()) {
            cancion.stop();
            cancion.release();
        }
    }

    public static void pausarFondo(MediaPlayer cancion) {

        try {

            if (null != cancion || cancion.isPlaying()) {
                cancion.pause();
            }
        } catch (IllegalStateException e) {
            cancion = null;
        }

    }

    public static void reproducirFondo(MediaPlayer cancion) {

        if (null != cancion) {
            cancion.start();
        }

    }

    public static void correcto(Context context) {
        MediaPlayer cancion = new MediaPlayer();

        cancion = MediaPlayer.create(context, R.raw.wonderful);
        cancion.start();

    }

    public static void incorrecto(Context context) {
        MediaPlayer cancion = new MediaPlayer();

        cancion = MediaPlayer.create(context, R.raw.bad);
        cancion.start();

    }

}
