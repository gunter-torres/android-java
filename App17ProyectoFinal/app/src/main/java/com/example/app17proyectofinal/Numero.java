package com.example.app17proyectofinal;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Numero {

    private Activity activity;

    private TextView tvJugador;
    private TextView tvPuntaje;

    private ImageView ivIntentos;
    private ImageView ivNumeroUno;
    private ImageView ivNumeroDos;
    private ImageView ivOperacion;

    private MediaPlayer sonido;

    private int nivel;
    private int intentos;
    private int puntuacion;
    private int resultado;
    private int puntosNivel;
    private int maximo;
    private int minimo;

    private Nivel area;

    private String jugador;
    private String operacion;

    public Numero(
            Activity activity,
            TextView tvJugador,
            TextView tvPuntaje,
            ImageView ivIntentos,
            ImageView ivNumeroUno,
            ImageView ivNumeroDos,
            ImageView ivOperacion,
            int nivel,
            int intentos,
            int puntuacion
    ) {

        this.activity = activity;
        this.tvJugador = tvJugador;
        this.tvPuntaje = tvPuntaje;
        this.ivIntentos = ivIntentos;
        this.ivNumeroUno = ivNumeroUno;
        this.ivNumeroDos = ivNumeroDos;
        this.ivOperacion = ivOperacion;
        this.nivel = nivel;
        this.intentos = intentos;
        this.puntuacion = puntuacion;

        sonido = Sonido.iniciarFondo(activity.getApplicationContext(), true);
        jugador = tvJugador.getText().toString();

        area = new Nivel(activity);

        // maximo depende del nivel
        switch (nivel) {
            case 1:
                minimo = 0;
                maximo = 10;
                puntosNivel = 10;
                operacion = "+";
                break;
            case 2:
                minimo = 10;
                maximo = 18;
                puntosNivel = 20;
                operacion = "+";
                break;
            case 3:
                minimo = 1;
                maximo = 9;
                puntosNivel = 30;
                operacion = "-";
                Imagen.ponerOperacion(activity, ivOperacion, operacion);
                break;
            case 4:
                minimo = 1;
                maximo = 45;
                puntosNivel = 40;
                operacion = "*";
                Imagen.ponerOperacion(activity, ivOperacion, operacion);
                break;
            case 5:
                minimo = 45;
                maximo = 81;
                puntosNivel = 50;
                operacion = "*";
                Imagen.ponerOperacion(activity, ivOperacion, operacion);
                break;
            case 6:
                minimo = 1;
                maximo = 81;
                puntosNivel = 100;
                break;
        }

        generar();

    }

    public void comparar(int respuesta) {

        if (resultado == respuesta) {
            Sonido.correcto(activity.getApplicationContext());
            puntuacion++;

            Datos.guardarRecord(activity.getApplicationContext(), jugador, puntuacion);

            if (puntuacion == puntosNivel) {

                pasarNivel();
                Sonido.detenerFondo(sonido);

                return;
            }

        } else {
            Sonido.incorrecto(activity.getApplicationContext());
            intentos -= 1;

            if (0 == intentos) {
                Sonido.detenerFondo(sonido);
                area.inicial();

                return;
            }

            Imagen.ponerIntentos(activity.getApplicationContext(), ivIntentos, intentos);
        }

        tvPuntaje.setText("Puntos: " + puntuacion);

        generar();

    }

    public void generar() {
        Context context = activity.getApplicationContext();

        if (6 == nivel) {
            operacionAleatoria();
            Imagen.ponerOperacion(activity, ivOperacion, operacion);
        }

        int numeroUno = ("-" == operacion) ? aleatorio(9, true) : aleatorio(9);
        // poner el numero maximo para el segundo numero
        int numeroDos = ("-" == operacion) ? aleatorio(numeroUno, true) : aleatorio(9);

        // poner la operacion;
        int resultado = operacion(numeroUno, numeroDos);

        if (resultado >= minimo && resultado <= maximo) {

            Imagen.ponerNumero(context, ivNumeroUno, numeroUno);
            Imagen.ponerNumero(context, ivNumeroDos, numeroDos);

            this.resultado = resultado;

        } else {
            generar();
        }

    }

    public void pausa() {
        Sonido.pausarFondo(sonido);
    }

    public void reproducir() {
        Sonido.reproducirFondo(sonido);
    }

    private int aleatorio(int max) {
        return (int) (Math.random() * 100) % max;
    }

    private int aleatorio(int max, boolean mayorCero) {
        int res = (int) (Math.random() * 100) % max;

        return (0 == res) ? max - 1 : res;
    }

    private int operacion(int uno, int dos) {
        int op = 0;

        switch (operacion) {
            case "+":
                op = uno + dos;
                break;
            case "-":
                op = uno - dos;
                break;
            case "*":
                op = uno * dos;
                break;
        }

        return op;
    }

    private void operacionAleatoria() {

        switch ((int) (Math.random() * 10) % 3) {
            case 0:
                operacion = "+";
                break;
            case 1:
                operacion = "-";
                break;
            case 2:
                operacion = "*";
                break;
        }

    }

    private void pasarNivel() {

        switch (nivel) {
            case 1:
                area.dos(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;
            case 2:
                // nivel 3
                area.tres(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;
            case 3:
                // nivel 4
                area.cuatro(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;
            case 4:
                // nivel 5
                area.cinco(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;
            case 5:
                // nivel 6
                area.seis(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;
            case 6:
                area.fin(new String[]{jugador, String.valueOf(puntuacion), String.valueOf(intentos)});
                break;

        }
    }

}
