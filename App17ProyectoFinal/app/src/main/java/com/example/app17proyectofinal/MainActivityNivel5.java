package com.example.app17proyectofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityNivel5 extends AppCompatActivity {

    private TextView tvJugador;
    private TextView tvPuntaje;

    private ImageView ivIntentos;
    private ImageView ivNumeroUno;
    private ImageView ivNumeroDos;
    private ImageView ivOperacion;

    private EditText etRespuesta;

    private int intentos;
    private int puntuacion;

    private String jugador;

    private Numero numero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_nivel5);

        tvJugador = findViewById(R.id.tvJugador);
        tvPuntaje = findViewById(R.id.tvPuntaje);
        ivIntentos = findViewById(R.id.ivIntentos);
        ivNumeroUno = findViewById(R.id.ivNumeroUno);
        ivNumeroDos = findViewById(R.id.ivNumeroDos);
        ivOperacion = findViewById(R.id.ivOperacion);
        etRespuesta = findViewById(R.id.etRespuesta);

        String nivel = "Nivel 5 - Multiplicación hasta 9";
        Toast.makeText(this, nivel, Toast.LENGTH_LONG).show();

        jugador = getIntent().getStringExtra("jugador");
        tvJugador.setText(jugador);

        String puntaje = getIntent().getStringExtra("puntuacion");
        puntuacion = Integer.parseInt(puntaje);
        tvPuntaje.setText("Puntos: " + puntuacion);

        String intento = getIntent().getStringExtra("intentos");
        intentos = Integer.parseInt(intento);

        Imagen.ponerIntentos(this, ivIntentos, intentos);

        numero = new Numero(
                this,
                tvJugador,
                tvPuntaje,
                ivIntentos,
                ivNumeroUno,
                ivNumeroDos,
                ivOperacion,
                5,
                intentos,
                puntuacion
        );

    }

    @Override
    protected void onResume() {

        super.onResume();
        numero.reproducir();

    }

    @Override
    protected void onPause() {

        super.onPause();
        numero.pausa();

    }

    public void comparar(View view) {
        String res = etRespuesta.getText().toString();

        if (res.isEmpty() || res.equals("-")) {
            Toast.makeText(this, "Ingresa un número", Toast.LENGTH_LONG).show();
        } else {
            int respuesta = Integer.parseInt(res);

            numero.comparar(respuesta);
            etRespuesta.setText("");

        }

    }

    @Override
    public void onBackPressed() {
        //nothing
    }
}