package com.example.app17proyectofinal;

import androidx.appcompat.app.AppCompatActivity;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Proyecto final de Curso Android desde Cero
 * Empieza en Video 50
 * de la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etJugador;
    private ImageView ivPersonaje;
    private TextView tvMejorPuntuacion;
    private MediaPlayer sonido;
    private static String singleton = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etJugador = findViewById(R.id.etJugador);
        ivPersonaje = findViewById(R.id.ivPersonaje);
        tvMejorPuntuacion = findViewById(R.id.tvMejorPuntuacion);
        //ponerIcono();

        Imagen.ponerFondo(this, ivPersonaje);
        sonido = Sonido.iniciarFondo(getApplicationContext(), false);
        mostrarPuntuacion();
        //singleton = singleton();
    }

    @Override
    protected void onResume() {

        super.onResume();
        sonido.start();

    }

    @Override
    protected void onStop() {

        super.onStop();
        if (null != sonido) {
            if (sonido.isPlaying()) {
                sonido.pause();
            }
        }
    }

    private static String singleton() {

        if (null == singleton) {
            singleton = new String("Iniciar");
        }

        return singleton;

    }

    private void mostrarPuntuacion() {
        String puntuacion;
        puntuacion = Datos.obtenerPuntuacion(getApplicationContext());

        tvMejorPuntuacion.setText(puntuacion);
    }

    private void ponerIcono() {
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);
    }


    public void jugar(View view) {
        String jugador = etJugador.getText().toString();
        Nivel nivel = new Nivel(this);

        if (jugador.isEmpty()) {
            String mensaje = "Ingresa el nombre del Jugador";
            Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();

            etJugador.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etJugador, InputMethodManager.SHOW_IMPLICIT);

        } else {
            Sonido.detenerFondo(sonido);
            sonido = null;
            // pasar al nivel 1
            nivel.uno(jugador);

        }

    }

    @Override
    public void onBackPressed() {
        //nothing
    }

}