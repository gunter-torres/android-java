package com.example.app17proyectofinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityFinal extends AppCompatActivity {

    private TextView tvJugadorFinal;
    private TextView tvMejorPuntuacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_final);

        tvJugadorFinal = findViewById(R.id.tvJugadorFinal);
        tvMejorPuntuacion = findViewById(R.id.tvMejorPuntuacion);

        String jugador = getIntent().getStringExtra("jugador");
        tvJugadorFinal.setText(jugador);

        String puntaje = getIntent().getStringExtra("puntuacion");
        int puntuacion = Integer.parseInt(puntaje);
        tvMejorPuntuacion.setText("Puntos: " + puntuacion);
    }

    public void iniciar(View view) {
        Nivel area = new Nivel(this);
        area.inicial();
    }

}