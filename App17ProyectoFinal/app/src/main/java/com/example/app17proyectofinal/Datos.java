package com.example.app17proyectofinal;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.app17proyectofinal.AdminSQLiteOpenHelper;

public class Datos {

    private static AdminSQLiteOpenHelper conexion;
    private static final String SQL = "SELECT nombre, MAX(puntuacion) AS puntos " +
            " FROM puntaje \n" +
            " GROUP BY nombre \n" +
            " ORDER BY puntos DESC\n" +
            " LIMIT 1 \n";

    public static String obtenerPuntuacion(Context context) {

        String respuesta = "";

        conexion = new AdminSQLiteOpenHelper(context, "BD", null, 1);
        SQLiteDatabase bd = conexion.getWritableDatabase();
        Cursor consulta = bd.rawQuery(SQL, null);

        if (consulta.moveToFirst()) {
            String nombre = consulta.getString(0);
            String puntaje = consulta.getString(1);

            respuesta = "Record: " + puntaje + " de " + nombre;
        }

        bd.close();

        return respuesta;
    }

    public static void guardarRecord(Context context, String nombre, int dato) {

        conexion = new AdminSQLiteOpenHelper(context, "BD", null, 1);
        SQLiteDatabase bd = conexion.getWritableDatabase();
        Cursor consulta = bd.rawQuery(SQL, null);
        ContentValues data = new ContentValues();
        data.put("nombre", nombre);
        data.put("puntuacion", dato);

        if (consulta.moveToFirst()) {
            String nombre1 = consulta.getString(0);
            int record = Integer.parseInt(consulta.getString(1));

            String condicion = " puntuacion = ? ";

            if (record < dato) {
                bd.update("puntaje", data, condicion, new String[]{"" + record});

            }

        } else {
            bd.insert("puntaje", null, data);
        }

        bd.close();

    }
}
