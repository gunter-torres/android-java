package com.example.app17proyectofinal;

import android.content.Context;
import android.content.Intent;
import android.app.Activity;

public class Nivel {

    private Activity activity;

    private Context context;

    private Intent intent;

    public Nivel(Activity activity) {
        this.activity = activity;

        setContext();
    }

    public void inicial() {

        intent = new Intent(this.context, MainActivity.class);

        finalizar();
    }

    public void uno(String dato) {

        intent = new Intent(context, MainActivityNivel1.class);
        intent.putExtra("jugador", dato);

        finalizar();

    }

    public void dos(String[] datos) {

        intent = new Intent(context, MainActivityNivel2.class);
        setData(datos);
        finalizar();

    }

    public void tres(String[] datos) {

        intent = new Intent(context, MainActivityNivel3.class);
        setData(datos);
        finalizar();

    }

    public void cuatro(String[] datos) {

        intent = new Intent(context, MainActivityNivel4.class);
        setData(datos);
        finalizar();

    }

    public void cinco(String[] datos) {

        intent = new Intent(context, MainActivityNivel5.class);
        setData(datos);
        finalizar();

    }

    public void seis(String[] datos) {

        intent = new Intent(context, MainActivityNivel6.class);
        setData(datos);
        finalizar();

    }

    public void fin(String[] datos) {

        intent = new Intent(context, MainActivityFinal.class);
        setData(datos);
        finalizar();

    }

    private void setContext() {
        context = activity.getApplicationContext();
    }

    private void setData(String[] data) {
        intent.putExtra("jugador", data[0]);
        intent.putExtra("puntuacion", data[1]);
        intent.putExtra("intentos", data[2]);
    }

    private void finalizar() {
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

        activity.finish();

    }

}
