package com.example.add11webview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Video 25 de Curso Android desde Cero
 * de la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUrl = findViewById(R.id.etUrl);

    }

    public void ir(View view) {

        String url = etUrl.getText().toString();

        if (url.isEmpty()) {
            Toast.makeText(this, "Ingresa un sitio web", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, WebActivity.class);

            intent.putExtra("url", url);

            startActivity(intent);
        }
    }
}