package com.example.add11webview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends AppCompatActivity {

    public WebView wvContenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        wvContenido = findViewById(R.id.wvContenido);
        String url = getIntent().getStringExtra("url");

        wvContenido.setWebViewClient(new WebViewClient());
        wvContenido.loadUrl("http://" + url);

    }

    public void cerrar(View view) {
        finish();
    }
}