package com.example.app12sharedpreferencesagenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Video 27 del Curso Android desde cero
 * de la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etNombre;
    private EditText etmDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.etNombre);
        etmDatos = findViewById(R.id.etmDatos);
    }

    public void guardar(View view) {
        String nombre = etNombre.getText().toString();
        String datos = etmDatos.getText().toString();

        if (nombre.isEmpty() && datos.isEmpty()) {
            Toast.makeText(this, "Ingrese el nombre y los datos", Toast.LENGTH_LONG).show();
        } else {

            SharedPreferences preferencias = getSharedPreferences("agenda", Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = preferencias.edit();

            edit.putString(nombre, datos);
            edit.commit();

            Toast.makeText(this, "Registro almacenado", Toast.LENGTH_LONG).show();
        }

    }

    public void buscar(View view) {
        String nombre = etNombre.getText().toString();

        if (nombre.isEmpty()) {
            Toast.makeText(this, "Ingrese el nombre", Toast.LENGTH_LONG).show();
        } else {
            SharedPreferences preferences = getSharedPreferences("agenda", Context.MODE_PRIVATE);
            String datos = preferences.getString(nombre, "");

            if (0 == datos.length()) {
                Toast.makeText(this, "Sin registros", Toast.LENGTH_SHORT).show();
            } else {
                etmDatos.setText(datos);
            }

        }


    }
}