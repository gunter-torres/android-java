package com.example.app2promedio;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.DeadObjectException;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText campo1;
    private EditText campo2;
    private EditText campo3;
    private TextView texto1;

    private Double getData(EditText campo) {
        String valor = campo.getText().toString();

        return valor.isEmpty() ? 0 : Double.parseDouble(valor);
    }

    public void promedio(View view) {
        Double promedio = (getData(campo1) + getData(campo2) + getData(campo3)) / 3;
        String mensaje = (promedio >= 6) ? "Aprobado" : "Reprobado";

        texto1.setText("Estatus " + mensaje + " con: " + promedio);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Asignar los campos

        campo1 = (EditText) findViewById(R.id.txtClase1);
        campo2 = (EditText) findViewById(R.id.txtClase2);
        campo3 = (EditText) findViewById(R.id.txtClase3);
        texto1 = (TextView) findViewById(R.id.tvEncabezado);

    }
}