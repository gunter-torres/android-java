package com.example.app18calculadorasalarial;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private static final int INSS = 7;
    private static final double MAXIMO_COTIZABLE_INSS = 96841.56;

    private EditText etSalario;

    private TextView tvDeduccionInss;
    private TextView tvDeduccionIr;
    private TextView tvDeduccionTotal;
    private TextView tvSalarioNeto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etSalario = findViewById(R.id.etSalario);
        tvDeduccionInss = findViewById(R.id.tvDeduccionInss);
        tvDeduccionIr = findViewById(R.id.tvDeduccionIr);
        tvDeduccionTotal = findViewById(R.id.tvDeduccionTotal);
        tvSalarioNeto = findViewById(R.id.tvSalarioNeto);

    }

    public void calcular(View view) {

        String texto = etSalario.getText().toString();

        if (texto.isEmpty() || texto.equals(".")) {
            return;
        }

        float salario = Float.parseFloat(texto);

        if (Float.isNaN(salario)) {
            return;
        }

        //float monto = (float) ((salario > MAXIMO_COTIZABLE_INSS) ? MAXIMO_COTIZABLE_INSS : salario);
        float inss = (salario * INSS) / 100;

        float ir = calcularIr(salario - inss);
        float deduccion = inss + ir;

        float neto = salario - deduccion;

        tvDeduccionInss.setText("C$" + customFormat("###,###.##", inss));
        tvDeduccionIr.setText("C$" + customFormat("###,###.##", ir));
        tvDeduccionTotal.setText("C$" + customFormat("###,###.##", deduccion));
        tvSalarioNeto.setText("C$" + customFormat("###,###.##", neto));

    }

    private float calcularIr(float salario) {

        float impuesto = 0;
        float porcentaje = 0;
        float excedente = 0;
        float ir = 0;

        salario *= 12;

        if (salario > 100000 && salario <= 200000) {
            porcentaje = 15;
            excedente = 100000;
        } else if (salario > 200000 && salario <= 350000) {
            impuesto = 15000;
            porcentaje = 20;
            excedente = 200000;
        } else if (salario > 350000 && salario <= 500000) {
            impuesto = 45000;
            porcentaje = 25;
            excedente = 350000;
        } else if (salario > 500000) {
            impuesto = 82500;
            porcentaje = 30;
            excedente = 500000;
        }

        if (0 != porcentaje) {
            float calculo = salario - excedente;
            calculo = impuesto + (calculo * porcentaje) / 100;
            ir = calculo / 12;
        }

        return ir;
    }

    private static String customFormat(String pattern, float value) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }

}