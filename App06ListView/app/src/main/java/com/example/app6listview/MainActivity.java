package com.example.app6listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Video 20 del Curso Android desde Cero
 * De la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {
    private TextView tvTexto;
    private ListView lvLista;

    private String[] nombre = {
            "Nombre 1",
            "Nombre 2",
            "Nombre 3",
            "Nombre 4",
            "Nombre 5",
            "Nombre 6"
    };

    private int[] edad = {20, 25, 30, 35, 40, 45};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTexto = findViewById(R.id.tvTexto);
        lvLista = findViewById(R.id.lvListado);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.list_item_listado,
                nombre
        );

        lvLista.setAdapter(adapter);

        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                tvTexto.setText(nombre[position] + " tiene " + edad[position] + " años");
            }
        });

    }
}