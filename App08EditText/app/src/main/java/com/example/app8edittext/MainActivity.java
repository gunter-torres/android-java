package com.example.app8edittext;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Video 22 del Curso Android desde Cero
 * De la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etNombre;
    private EditText etClave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.txtNombre);
        etClave = findViewById(R.id.txtClave);

    }

    public void registrar(View view){
        String nombre = etNombre.getText().toString();
        String clave = etClave.getText().toString();

        if(nombre.isEmpty() || 0 == nombre.length() ){
            Toast.makeText(this, "Ingresa el nombre", Toast.LENGTH_LONG).show();
        }else if(clave.isEmpty() || 0 == clave.length()){
            Toast.makeText(this, "Ingresa el clave", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Registro en proceso ...", Toast.LENGTH_LONG).show();
        }
    }
}