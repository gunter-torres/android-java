package com.example.app15sqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Video 30, 31, 32 y 33 del Curso Android desde Cero
 * de la Geekipedia de Ernesto
 */

public class MainActivity extends AppCompatActivity {

    private static final String BD_NOMBRE = "administracion";
    private static final String BD_TABLA = "articulos";
    private static final int BD_VERSION = 1;

    private EditText etCodigo;
    private EditText etDescripcion;
    private EditText etPrecio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etCodigo = findViewById(R.id.etCodigo);
        etDescripcion = findViewById(R.id.etDescripcion);
        etPrecio = findViewById(R.id.etPrecio);
    }

    public void registrar(View view) {

        String mensaje = "";
        String codigo = etCodigo.getText().toString();
        String descripcion = etDescripcion.getText().toString();
        String precio = etPrecio.getText().toString();

        if (codigo.isEmpty() || descripcion.isEmpty() || precio.isEmpty()) {
            mensaje = "Ingresa todos los datos a guardar";
        } else {

            AdminSQLiteOpenHelper adminDb = new AdminSQLiteOpenHelper
                    (this, BD_NOMBRE, null, BD_VERSION);

            SQLiteDatabase db = adminDb.getWritableDatabase();

            String condicion = " codigo = ?";

            String sql = "SELECT descripcion, precio \n";
            sql += " FROM " + BD_TABLA;
            sql += " WHERE " + condicion;

            ContentValues fila = new ContentValues();

            fila.put("descripcion", descripcion);
            fila.put("precio", precio);

            Cursor buscar = db.rawQuery(sql, new String[]{codigo});

            if (0 == buscar.getCount()) {
                // guardar por primera vez
                fila.put("codigo", codigo);
                db.insert(BD_TABLA, null, fila);

                mensaje = "Ingresado correctamente";

                etCodigo.setText("");
                etDescripcion.setText("");
                etPrecio.setText("");

            } else {
                //actualizar
                try {
                    db.update(BD_TABLA, fila, condicion, new String[]{codigo});
                    mensaje = "Modificado correctamente";
                } catch (SQLiteException e) {
                    mensaje = e.toString();
                }

            }

            db.close();
            adminDb.close();

        }

        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

    public void buscar(View view) {
        String codigo = etCodigo.getText().toString();
        String mensaje = "";
        boolean mostrar = true;

        if (codigo.isEmpty()) {
            mensaje = "Ingresa el código a buscar";

        } else {

            String sql = "SELECT descripcion, precio \n";
            sql += " FROM " + BD_TABLA;
            sql += " WHERE codigo = ?";

            AdminSQLiteOpenHelper adminDB = new AdminSQLiteOpenHelper
                    (this, BD_NOMBRE, null, BD_VERSION);

            SQLiteDatabase db = adminDB.getWritableDatabase();

            Cursor fila = db.rawQuery(sql, new String[]{codigo});

            if (fila.moveToFirst()) {
                etDescripcion.setText(fila.getString(0));
                etPrecio.setText(fila.getString(1));
                mostrar = false;

            } else {
                mensaje = "Código no encontrado";

            }

            db.close();
            adminDB.close();

        }
        if (mostrar) {
            Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
        }
    }

    public void eliminar(View view) {

        String codigo = etCodigo.getText().toString();
        String mensaje = "";

        if (codigo.isEmpty()) {
            mensaje = "Ingresa el código a buscar";

        } else {

            String condicion = " codigo = ?";

            AdminSQLiteOpenHelper adminDb = new AdminSQLiteOpenHelper
                    (this, BD_NOMBRE, null, BD_VERSION);

            SQLiteDatabase db = adminDb.getWritableDatabase();

            int cantidad = db.delete(BD_TABLA, condicion, new String[]{codigo});

            db.close();
            adminDb.close();

            etCodigo.setText("");
            etDescripcion.setText("");
            etPrecio.setText("");

            mensaje = (1 == cantidad) ? "Producto eliminado" : "Producto no existe";

        }

        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }

}