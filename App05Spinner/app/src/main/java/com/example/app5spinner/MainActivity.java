package com.example.app5spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Video 18 del Curso Android desde Cero
 * De la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private Spinner opciones;
    private EditText valor1;
    private EditText valor2;
    private TextView texto;
    private String[] operacion = {
            "Sumar",
            "Restar",
            "Multiplicar",
            "Dividir"
    };

    private Double getData(EditText campo) {
        String valor = campo.getText().toString();

        return valor.isEmpty() ? 0 : Double.parseDouble(valor);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        valor1 = findViewById(R.id.txtValor1);
        valor2 = findViewById(R.id.txtValor2);
        texto = findViewById(R.id.tvTexto);
        opciones = findViewById(R.id.spOpciones);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this,
                R.layout.spinner_item,
                operacion
        );

        opciones.setAdapter(adapter);

    }

    public void Calcular(View view) {
        Double val1 = getData(valor1);
        Double val2 = getData(valor2);
        Double res = 0.0;

        String seleccion = opciones.getSelectedItem().toString();

        if (seleccion.equals(operacion[0])) {
            res = val1 + val2;
        }

        if (seleccion.equals(operacion[1])) {
            res = val1 - val2;
        }

        if (seleccion.equals(operacion[2])) {
            res = val1 * val2;
        }

        if (seleccion.equals(operacion[3])) {
            if (0 == val2) {
                Toast.makeText(this, "División por cero sin definición", Toast.LENGTH_LONG)
                        .show();
            } else {
                res = val1 / val2;
            }
        }

        texto.setText((0 == val2) ? "" : res.toString());

    }
}