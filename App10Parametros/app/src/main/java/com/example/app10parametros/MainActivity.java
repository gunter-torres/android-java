package com.example.app10parametros;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Video 24 des Curso de Android
 * De la Geekipedia de Ernesto
 */

public class MainActivity extends AppCompatActivity {

    private EditText etNombre1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre1 = findViewById(R.id.etNombre1);
    }

    public void enviar(View view) {

        String data = etNombre1.getText().toString();

        if (data.isEmpty()) {
            Toast.makeText(this, "Ingresa un nombre", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, MainActivity2.class);

            intent.putExtra("data", data);

            startActivity(intent);
        }
    }
}