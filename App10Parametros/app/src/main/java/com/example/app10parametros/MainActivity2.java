package com.example.app10parametros;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    private TextView tvNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        tvNombre = findViewById(R.id.tvNombre);

        String data = getIntent().getStringExtra("data");

        tvNombre.setText("Hola " + data);
    }

    public void regresar(View view) {
        Intent intent = new Intent(this, MainActivity.class);

        startActivity(intent);
    }
}