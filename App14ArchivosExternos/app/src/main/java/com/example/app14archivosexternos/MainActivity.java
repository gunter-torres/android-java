package com.example.app14archivosexternos;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Video 29 del Curso de Android desde cero
 * De la Geekipedia de Ernesto
 */
public class MainActivity extends AppCompatActivity {

    private EditText etNombre;
    private EditText etContenido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombre = findViewById(R.id.etNombre);
        etContenido = findViewById(R.id.etContenido);

    }

    public void guardar(View view) {
        String nombre = etNombre.getText().toString();
        String contenido = etContenido.getText().toString();
        String mensaje;

        if (nombre.isEmpty() && contenido.isEmpty()) {
            mensaje = "Ingrese los datos a guardar";
        } else {
            try {
                File tarjetaSd = Environment.getExternalStorageDirectory();
                mensaje = tarjetaSd.getPath() + "\n";

                File ruta = new File(tarjetaSd.getPath(), nombre);

                OutputStreamWriter archivo = new OutputStreamWriter(
                        openFileOutput(nombre, Activity.MODE_PRIVATE)
                );

                archivo.write(contenido);
                archivo.flush();
                archivo.close();

                mensaje += "Guardado correctamente";
                etContenido.setText("");
                etContenido.setText("");

            } catch (IOException e) {
                mensaje = "No se pudo guardar";
            }

        }

        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }

    public void consultar(View view) {
        String nombre = etNombre.getText().toString();
        String mensaje;
        String contenido = "";

        try {
            File tarjetaSd = Environment.getExternalStorageDirectory();
            File ruta = new File(tarjetaSd.getPath(), nombre);
            InputStreamReader archivo = new InputStreamReader(openFileInput(nombre));

            BufferedReader buffer = new BufferedReader(archivo);

            String linea = buffer.readLine();

            while (linea != null) {
                contenido += linea + "\n";
                linea = buffer.readLine();
            }

            buffer.close();
            archivo.close();

            etContenido.setText(contenido);

        } catch (IOException e) {
            mensaje = "Error al leer el archivo";
            Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
        }


    }
}