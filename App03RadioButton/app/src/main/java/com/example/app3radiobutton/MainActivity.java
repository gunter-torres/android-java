package com.example.app3radiobutton;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText campo1;
    private EditText campo2;
    private TextView resultado;
    private RadioButton rbSuma;
    private RadioButton rbResta;
    private RadioButton rbMultiplica;
    private RadioButton rbDivide;

    private Double getData(EditText campo) {
        String valor = campo.getText().toString();

        return valor.isEmpty() ? 0 : Double.parseDouble(valor);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        campo1 = findViewById(R.id.txtCampo1);
        campo2 = findViewById(R.id.txtCampo2);
        resultado = findViewById(R.id.tvResultado);
        rbSuma = findViewById(R.id.rbSuma);
        rbResta = findViewById(R.id.rbResta);
        rbMultiplica = findViewById(R.id.rbMultiplica);
        rbDivide = findViewById(R.id.rbDivide);

    }

    public void Calcular(View view) {
        Double operacion = 0.0;
        Double valor1 = getData(campo1);
        Double valor2 = getData(campo2);
        Boolean indefinido = false;
        String mensaje;

        if (rbSuma.isChecked()) {
            operacion = valor1 + valor2;
        }

        if (rbResta.isChecked()) {
            operacion = valor1 - valor2;
        }

        if (rbMultiplica.isChecked()) {
            operacion = valor1 * valor2;
        }

        if (rbDivide.isChecked()) {
            if (valor2 != 0) {
                operacion = valor1 / valor2;
            } else {
                indefinido = true;
            }
        }

        if (indefinido) {
            mensaje = "División por cero sin definición";
        } else {
            mensaje = operacion.toString();
        }

        resultado.setText(mensaje);

    }
}